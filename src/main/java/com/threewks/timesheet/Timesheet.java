package com.threewks.timesheet;

import java.util.List;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Cache
@Entity
public class Timesheet {
	@Id public Long id;
	@Embed public List<Entry> entries;
}
