package com.threewks.timesheet.controller;

import java.util.HashMap;
import java.util.Map;

import com.threewks.thundr.http.exception.NotFoundException;
import com.threewks.thundr.view.jsp.JspView;
import com.threewks.thundr.view.json.JsonView;
import com.threewks.timesheet.Timesheet;
import com.threewks.timesheet.service.TimesheetService;

public class TimesheetController {
	private TimesheetService timesheetService;

	public TimesheetController(TimesheetService timesheetService) {
		this.timesheetService = timesheetService;
	}

	public JspView list() {
		return new JspView("timesheet.jsp");
	}

	public JsonView get(Long id) {
		Timesheet timesheet = timesheetService.get(id);
		if (timesheet == null)
			throw new NotFoundException("No timesheet with id " + id);
		return new JsonView(timesheet);
	}

	public JsonView put(Timesheet timesheet) {
		return new JsonView(timesheetService.put(timesheet));
	}
}
