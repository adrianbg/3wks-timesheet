package com.threewks.timesheet;

import com.googlecode.objectify.ObjectifyService;
import com.threewks.thundr.injection.BaseInjectionConfiguration;
import com.threewks.thundr.injection.UpdatableInjectionContext;
import com.threewks.timesheet.service.TimesheetService;
import com.threewks.timesheet.service.TimesheetServiceImpl;

public class TimesheetInjectionConfiguration extends BaseInjectionConfiguration {
	@Override
	protected void addServices(UpdatableInjectionContext injectionContext) {
		injectionContext.inject(TimesheetServiceImpl.class).as(TimesheetService.class);

		configureObjectify();
		super.addServices(injectionContext);
	}

	private void configureObjectify() {
		ObjectifyService.register(Timesheet.class);
	}
}
