package com.threewks.timesheet.service;

import static com.googlecode.objectify.ObjectifyService.ofy;

import com.threewks.timesheet.Timesheet;
import com.threewks.timesheet.Entry;

public class TimesheetServiceImpl implements TimesheetService {
	public Timesheet get(Long id) {
		return ofy().load().type(Timesheet.class).id(id).get();
	}

	public Timesheet put(Timesheet timesheet) {
		ofy().save().entity(timesheet).now();
		return timesheet;
	}
}
