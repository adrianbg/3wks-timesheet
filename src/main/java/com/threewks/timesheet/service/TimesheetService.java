package com.threewks.timesheet.service;

import com.threewks.timesheet.Timesheet;
import com.threewks.timesheet.Entry;

public interface TimesheetService {
	public Timesheet get(Long id);
	public Timesheet put(Timesheet timesheet);
}
