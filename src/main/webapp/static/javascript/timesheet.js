var timesheet = angular.module('timesheet', ['ngResource', 'ngCookies'])

timesheet.controller('TimesheetCtrl', function($scope, $resource, $cookieStore) {
	var Timesheet = $resource('timesheet/:id', {id: '@id'});

	$scope.days = [
		'Monday',
		'Tuesday',
		'Wednesday',
		'Thursday',
		'Friday',
		'Saturday',
		'Sunday'
	];

	if ($cookieStore.get('timesheet_id'))
		$scope.timesheet = Timesheet.get({id: $cookieStore.get('timesheet_id')})
	else {
		$scope.timesheet = new Timesheet();
		$scope.timesheet.id = null;

		$scope.timesheet.entries = new Array($scope.days.length);
		for (var i=0; i < $scope.timesheet.entries.length; i++)
			$scope.timesheet.entries[i] = {start: '', end: ''};
	}

	$scope.save = function(timesheet) {
		timesheet.$save(null, function() {
			$cookieStore.put('timesheet_id', timesheet.id);
		});
	};
});
