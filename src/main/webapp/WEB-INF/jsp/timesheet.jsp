<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" uri="http://threewks.com/thundr/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags"%>
<tags:header id="timesheet" ngapp="timesheet" />
<div ng-controller="TimesheetCtrl">
	<div class="row">
		<div class="span1"></div>
		<div ng-repeat="day in days" class="span1">{{day}}</div>
	</div>
	<div class="row">
		<div class="span1">Start</div>
			<div ng-repeat="entry in timesheet.entries" class="span1">
			<input type="text" class="input-mini" ng-model="entry.start" placeholder="hh:mm" />
		</div>
	</div>
	<div class="row">
		<div class="span1">End</div>
		<div ng-repeat="entry in timesheet.entries" class="span1">
			<input type="text" class="input-mini" ng-model="entry.end" placeholder="hh:mm" />
		</div>
	</div>
	<button class="btn" ng-click="save(timesheet)">Save</button>
</div>
<t:script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.5/angular.min.js" />
<t:script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.5/angular-resource.min.js" />
<t:script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.5/angular-cookies.min.js" />
<t:script src="timesheet.js" />
<tags:footer />
